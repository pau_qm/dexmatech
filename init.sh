#!/bin/bash

distrib=$(cat /etc/*release | grep ID_LIKE | cut -d'=' -f2)
vagrant_file="vagrant_2.2.14_x86_64.deb"
helm_file="helm-v3.5.2-linux-amd64.tar.gz"

if [ "$distrib" == "debian" ]; then
    user=$(whoami)
    if [ "$user" == "root" ]; then
        # Install required software
        echo "Installing all required software.."
        apt-add-repository -y ppa:ansible/ansible
        apt-get update
        apt-get install -y ansible virtualbox wget dpkg
        wget https://releases.hashicorp.com/vagrant/2.2.14/${vagrant_file}
        dpkg -i $vagrant_file
        rm -f $vagrant_file

        wget https://get.helm.sh/${helm_file}
        tar -zxvf $helm_file
        mv linux-amd64/helm /usr/local/bin/helm
        rm -rf $helm_file linux-amd64

        # Run Ansible script to configure Vagrant and install Kubernetes based on official documentation: 
        # https://kubernetes.io/blog/2019/03/15/kubernetes-setup-using-ansible-and-vagrant/
        # With 1 node instead of 2
        # And build and deploy Redis service and client with Helm Chart
        echo "Running script to create Kubernetes in Vagrant, and deploy all services"
        ansible-playbook deploy-playbook.yml
    else
        echo "Run script as root user"
    fi
else
    echo "Run in Debian/Ubuntu distribution"
fi